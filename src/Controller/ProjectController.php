<?php

// 1. Исправить на App\Controller
namespace Api\Controller;

use App\Model;
use App\Storage\DataStorage;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// 1. Стоит формализовать тип ответа. Например для success:
// ['data' => *тело ответа*, 'success' => true]
// ['error' => ['message' => *сообщение*, 'code' => *код*], 'success' => false]
class ProjectController
{
    // 1. Зависимости лучше передавать по имени интерфейса
    // 2. Не указан тип возвращаемого значения
    /**
     * @var DataStorage
     */
    private $storage;

    public function __construct(DataStorage $storage)
    {
        $this->storage = $storage;
    }

    // 1. $id можно получить параметром при использованнии symfony/routing
    // 2. Не указан тип возвращаемого значения
    /**
     * @param Request $request
     *
     * @Route("/project/{id}", name="project", method="GET")
     */
    public function projectAction(Request $request)
    {
        try {
            $project = $this->storage->getProjectById($request->get('id'));

            // 1. Лучше использовать JsonResponse
            // 2. Формат сообщения. Подробно в начале
            return new Response($project->toJson());
        } catch (Model\NotFoundException $e) {
            // 1. Лучше использовать JsonResponse
            // 2. Формат сообщения. Подробно в начале
            return new Response('Not found', 404);
        } catch (\Throwable $e) {
            // 1. Лучше использовать JsonResponse
            // 2. Формат сообщения. Подробно в начале
            return new Response('Something went wrong', 500);
        }
    }

    // 1. $id можно получить параметром при использованнии symfony/routing
    // 2. Не указан тип возвращаемого значения
    /**
     * @param Request $request
     *
     * @Route("/project/{id}/tasks", name="project-tasks", method="GET")
     */
    public function projectTaskPagerAction(Request $request)
    {
        $tasks = $this->storage->getTasksByProjectId(
            $request->get('id'),
            $request->get('limit'),
            $request->get('offset')
        );

        // 1. Лучше использовать JsonResponse
        // 2. Формат сообщения. Подробно в начале
        return new Response(json_encode($tasks));
    }

    /**
     * @param Request $request
     *
     * @Route("/project/{id}/tasks", name="project-create-task", method="PUT")
     */
    public function projectCreateTaskAction(Request $request)
    {
		$project = $this->storage->getProjectById($request->get('id'));
		if (!$project) {
            // 1. Формат сообщения. Подробно в начале
			return new JsonResponse(['error' => 'Not found']);
		}

        // 1. Формат сообщения. Подробно в начале
        // 2. Лишняя глобальная переменная. У нас уже есть объект Request
		return new JsonResponse(
			$this->storage->createTask($_REQUEST, $project->getId())
		);
    }
}
