<?php

namespace App\Model;

// 1. Стоит выделить интерфейс под модели и отнаследовать его от JsonSerializable
// 2. Вынести в абстрактную модель конструктор, jsonSerialize и toJson (если он не покажется избыточным при реализации JsonSerializable)
class Task implements \JsonSerializable
{
    /**
     * @var array
     */
    private $_data;

    public function __construct($data)
    {
        $this->_data = $data;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return $this->_data;
    }
}
