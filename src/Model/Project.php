<?php

namespace App\Model;

// 1. Стоит выделить интерфейс под модели и отнаследовать его от JsonSerializable
// 2. Вынести в абстрактную модель конструктор, jsonSerialize и toJson (если он не покажется избыточным при реализации JsonSerializable)
class Project
{
    /**
     * @var array
     */
    public $_data;

    public function __construct($data)
    {
        $this->_data = $data;
    }

    // 1. Не указан тип возвращаемого значения
    /**
     * @return int
     */
    public function getId()
    {
        return (int) $this->_data['id'];
    }

    // 1. Не указан тип возвращаемого значения
    /**
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->_data);
    }
}
