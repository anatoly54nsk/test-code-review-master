<?php

namespace App\Storage;

use App\Model;

class DataStorage
{
    // 1. PDO лучше передавать зависимостью. В идеале врапнуть в класс имплиментирующий интерфейс работы с бд
    // 2. Поле $pdo должно быть приватным, т.к.
    // 3. Отсутствует ext-pdo в composer.json
    // 4. Перенести импорт класса в блок use
    /**
     * @var \PDO
     */
    public $pdo;

    public function __construct()
    {
        $this->pdo = new \PDO('mysql:dbname=task_tracker;host=127.0.0.1', 'user');
    }

    // 1. Не указан тип данных $projectId
    // 2. Не указан тип возвращаемого значения
    /**
     * @param int $projectId
     * @throws Model\NotFoundException
     */
    public function getProjectById($projectId)
    {
        // 1. SQL-инъекция. Стоит использовать подготовленный запрос
        // 2. Приведение типа к int не имеет смысла, т.к. тип параметра уже int
        $stmt = $this->pdo->query('SELECT * FROM project WHERE id = ' . (int) $projectId);

        if ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            // 1. Стоит использвать фабрику для создания моделей
            return new Model\Project($row);
        }

        throw new Model\NotFoundException();
    }

    // 1. Не указан тип данных $limit
    // 2. Не указан тип данных $offset
    // 3. Не указан тип возвращаемого значения
    /**
     * @param int $project_id
     * @param int $limit
     * @param int $offset
     */
    public function getTasksByProjectId(int $project_id, $limit, $offset)
    {
        // 1. SQL-инъекция. $project_id стоит заменить ? и добавить в массив параметров в execute
        $stmt = $this->pdo->query("SELECT * FROM task WHERE project_id = $project_id LIMIT ?, ?");
        // 1. Стоит добавить проверку, как прошел execute перед фетчем
        $stmt->execute([$limit, $offset]);

        $tasks = [];
        foreach ($stmt->fetchAll() as $row) {
            // 1. Стоит использвать фабрику для создания моделей
            $tasks[] = new Model\Task($row);
        }

        return $tasks;
    }

    // 1. Не указан тип данных $projectId
    // 2. Не указан тип возвращаемого значения
    /**
     * @param array $data
     * @param int $projectId
     * @return Model\Task
     */
    public function createTask(array $data, $projectId)
    {
        // 1. Не валидируются данные
        $data['project_id'] = $projectId;

        $fields = implode(',', array_keys($data));
        // 1. Лучше заменить на конактенацию ? по количеству параметров и передачу значений $data в execute
        //  во избежании SQL-инъекций
        $values = implode(',', array_map(function ($v) {
            return is_string($v) ? '"' . $v . '"' : $v;
        }, $data));

        // 1. Отсутствует execute для инстерта
        $this->pdo->query("INSERT INTO task ($fields) VALUES ($values)");
        $data['id'] = $this->pdo->query('SELECT MAX(id) FROM task')->fetchColumn();

        // 1. Стоит использвать фабрику для создания моделей
        return new Model\Task($data);
    }
}
